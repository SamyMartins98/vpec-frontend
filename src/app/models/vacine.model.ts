import { JSONModel } from './../utils/json.model';

export class Vacine extends JSONModel {
  id?: number;
  name?: string | null;
  dateDueDate?: number;
  vacineType?: number;
  situation?: boolean;


  isActiveInactive(): boolean {
      return this.situation === true;
  }

}

export interface IVacine {
  name?: string | null;
  dateDueDate?: number;
  situation?: boolean;
  vacineType?: string | null;
}

export class VacineType extends JSONModel {
  id?: number;
  name?: string | null;
  description?: string | null;
}
