import { Farm } from './../../../../models/farm.model';
import { Component, ElementRef, ViewChild } from '@angular/core';

import { AuthService } from 'src/app/services/auth.service';
import { FarmService } from 'src/app/services/farm.service';
import { DataTableItems } from '../../shared/data-table/data-table.component';

@Component({
  selector: 'app-farm-list',
  templateUrl: './farm-list.component.html',
  styleUrls: ['./farm-list.component.css'],
})
export class FarmListComponent {


  title: string = 'Fazendas';
  data: DataTableItems[];
  canCreate?: boolean;

  constructor(
    public service: FarmService,
    private authService: AuthService
  ) {
    this.data = [
      { name: 'id', field: 'id', type: 'text', showOnMobile: true },
      { name: 'nome', field: 'name', type: 'text', showOnMobile: true },
      { name: 'cidade', field: 'city', type: 'text' },
      { name: 'ativo', field: 'activeInative', type: 'boolean' }

    ];
    this.canCreate = this.authService.user?.hasUserTypes(['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO']);

  }

}
