import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class AlertService {

    constructor(
        private toast: ToastrService
    ) { }

    success(message: string, title: string = 'Sucesso!'): void {
        this.clear();
        this.toast.success(message, title);
    }

    error(message: string, title: string = 'Algo errado!'): void {
        this.clear();
        this.toast.error(message, title);
    }

    warning(message: string, title: string = 'Atenção!'): void {
        this.clear();
        this.toast.warning(message, title);
    }

    async serverError(error: any): Promise<void> {
        this.clear();
        if (error && error.error) {
            if (error.error.size) {
                error.error = await new Promise((resolve) => {
                    const reader = new FileReader();
                    reader.onloadend = () => resolve(JSON.parse(reader.result as string));
                    reader.readAsText(error.error);
                });
            }

            if (error.error.message) {
                this.toast.error(error.error.message);
            }
        }
    }

    private clear(): void {
        this.toast.clear();
    }
}
