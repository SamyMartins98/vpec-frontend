import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormController } from 'src/app/utils/form.controller';
import { ConfirmPasswordStateMatcher, ConfirmPasswordValidator } from 'src/app/validators/confirmPassword.validator';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent extends FormController {

  confirmPasswordStateMatcher = new ConfirmPasswordStateMatcher();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private alertService: AlertService,
    private route: ActivatedRoute
  ) {
    super();
    const token = this.route.snapshot.queryParams.token;
    if (!token) {
      this.alertService.error('Código inválido');
      this.authService.goHome();
    } else {
      this.form = this.formBuilder.group({
        token: [token, [Validators.required]],
        password: ['', [Validators.required]],
        confirm_password: ['', [Validators.required]],
      }, { validators: ConfirmPasswordValidator('password', 'confirm_password') });
    }
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      await this.authService.changePassword(this.form.value);
      this.alertService.success('Senha alterada com sucesso!');
      await this.authService.goHome();
    }
  }
}
