
import { Injectable } from '@angular/core';
import { CanLoad, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanLoad, CanActivate {
    constructor(
        private authService: AuthService
    ) { }

    canLoad(): boolean {
        return this.isLogged();
    }

    canActivate(): boolean {
        return this.isLogged();
    }

    isLogged(): boolean {
        if (this.authService.user) {
            return true;
        } else {
            this.authService.goHome();
            return false;
        }
    }
}
