export type UserType = 'ROLE_ADMINISTRADOR' | 'ROLE_FAZENDEIRO';
export type Permission = { [key: string]: { name?: string, permissions: UserType[] } };
