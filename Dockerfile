FROM node:14

RUN npm install -g @angular/cli@11.0.3

WORKDIR /app
COPY . ./

COPY entrypoint.sh /usr/local/bin/
ENTRYPOINT ["entrypoint.sh"]

EXPOSE 4200
CMD ng serve --host 0.0.0.0 --poll 1000 --disableHostCheck