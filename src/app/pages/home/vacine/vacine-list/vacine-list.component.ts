import { Component, OnInit } from '@angular/core';
import { Vacine } from 'src/app/models/vacine.model';
import { AuthService } from 'src/app/services/auth.service';
import { VacineService } from 'src/app/services/vacine.service';
import { DataTableItems } from '../../shared/data-table/data-table.component';

@Component({
  selector: 'app-vacine-list',
  templateUrl: './vacine-list.component.html',
  styleUrls: ['./vacine-list.component.css']
})
export class VacineListComponent implements OnInit {

  ngOnInit(): void {
  }

  title: string = 'Vacinas';
  data: DataTableItems[];
  canCreate?: boolean;

  constructor(
    public service: VacineService,
    private authService: AuthService
  ) {
    this.data = [
      { name: 'id', field: 'id', type: 'text', showOnMobile: true },
      { name: 'nome', field: 'name', type: 'text', showOnMobile: true }

    ];
    this.canCreate = this.authService.user?.hasUserTypes(['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO']);

  }

}
