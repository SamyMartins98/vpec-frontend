import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Management } from 'src/app/models/management.model';
import { IVacine, Vacine } from 'src/app/models/vacine.model';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { ManagementService } from 'src/app/services/management.service';
import { VacineService } from 'src/app/services/vacine.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-management-detail',
  templateUrl: './management-detail.component.html',
  styleUrls: ['./management-detail.component.css']
})
export class ManagementDetailComponent extends FormController implements OnInit {

  management!: Management;
  managementId: number = 0;
  canEdit?: boolean;
  canShow?: boolean;
  vacineName?: string | null;
  vacine!: Vacine;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private managementService: ManagementService,
    private authService: AuthService,
    private vacineService: VacineService
  ) {
    super([
      'id',
      'description',
      'dateManagement',
      'timeCourse',
      'vacineName'
    ]);
    this.managementId = this.route.snapshot.params.id;

   }

   async ngOnInit(): Promise<void> {
    try {
      this.management = await this.managementService.get(this.managementId);
      this.vacine = await this.vacineService.get(this.management.vacineId);
      this.vacineName = this.vacine.name;
    } catch (error) {
      this.router.navigate(['..'], { relativeTo: this.route });
    }
    this.updateForm();
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      this.management = await this.managementService.update(this.managementId, this.form.value);
      this.updateForm();
      this.alertService.success('Dados atualizados com sucesso!');
    }
  }

  updateForm(): void {
    this.form = this.formBuilder.group({
      id: [{ value: this.management.id, disabled: true }, [Validators.required]],
      description: [{ value: this.management.description, disabled: this.canEdit }, [Validators.required]],
      dateManagement: [{ value: this.management.dateManagement, disabled: !this.canEdit }, [Validators.required]],
      timeCourse: [{ value: this.management.timeCourse, disabled: this.canEdit }, [Validators.required]],
      vacineName: [{ value: this.vacineName, disabled: !this.canEdit }, [Validators.required]],
    });
  }

  isPresente(){
    if(this.management.dateManagement != null)
      return true;
    else
      return false;
  }

}
