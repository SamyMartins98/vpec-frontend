import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { City } from 'src/app/models/city.model';
import { AlertService } from 'src/app/services/alert.service';
import { CityService } from 'src/app/services/city.service';
import { FarmService } from 'src/app/services/farm.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-farm-create',
  templateUrl: './farm-create.component.html',
  styleUrls: ['./farm-create.component.css']
})

export class FarmCreateComponent extends FormController implements OnInit{

  citys: City[] = [];

  tipoDocumento: string = '';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private farmService: FarmService,
    private cityService: CityService
  ) {
    super(['city', 'name']);
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      city: ['', [Validators.required]]
    });


  }

  ngOnInit(){
    this.findCitys();
  }

  findCitys(){
    this.cityService.all().then(r => {
      this.citys = r;
    });
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      const farm = await this.farmService.register(this.form.value);
      this.router.navigate(['..', farm.id], { relativeTo: this.route });
      this.alertService.success('Fazenda criada com sucesso!');
    }
  }

}
