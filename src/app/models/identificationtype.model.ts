import { JSONModel } from "../utils/json.model";

export class IdentificationType extends JSONModel {
  id?: number;
  name?: string | null;
  description?: string | null;

}

export interface IIdentificationType {
  name?: string | null;
  description?: string | null;
}
