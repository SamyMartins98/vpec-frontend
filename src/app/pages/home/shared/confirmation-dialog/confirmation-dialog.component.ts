import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

type DialogData = {
  title?: string,
  message?: string,
  cancel?: string,
  ok?: string
};

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent {
  title: string = 'Você tem certeza disso?';
  message: string = 'Essa ação é irreversível, se você continuar não poderá mais desfazer essa ação!';
  cancel: string = 'CANCELAR';
  ok: string = 'SIM';

  constructor(@Inject(MAT_DIALOG_DATA) private data: DialogData) {
    if (this.data?.title !== undefined) {
      this.title = this.data.title;
    }

    if (this.data?.message !== undefined) {
      this.message = this.data.message;
    }

    if (this.data?.cancel) {
      this.cancel = this.data.cancel;
    }

    if (this.data?.ok) {
      this.ok = this.data.ok;
    }
  }
}
