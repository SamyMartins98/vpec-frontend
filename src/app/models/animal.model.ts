import { JSONModel } from '../utils/json.model';

export class Animal extends JSONModel {
  id?: number;
  sex?: string | null;
	birth?: number;
	nameColor?: string | null;
	qtFirstWeight?: number;
	dateFirstWeight?: number;
	castrated?: boolean;
	farmInput?: number;
	coat?: boolean;
	descriptionDeathOrLoss?: string | null;
	dateDeathOrLoss?: number;
	identificationType?: number;
	batch?: number;

}

export interface IAnimal {
  management?: number;
  sex?: string | null;
	birth?: number;
	nameColor?: string | null;
	qtFirstWeight?: number;
	dateFirstWeight?: number;
	castrated?: boolean;
	farmInput?: number;
	coat?: boolean;
	descriptionDeathOrLoss?: string | null;
	dateDeathOrLoss?: number;
	identificationType?: number;
	batch?: number;
}
