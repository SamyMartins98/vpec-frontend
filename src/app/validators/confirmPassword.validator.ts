import { AbstractControl, FormControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

export function ConfirmPasswordValidator(passwordField: string, confirmPasswordField: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        if (control) {
            const password = control.get(passwordField)?.value;
            const confirmPassword = control.get(confirmPasswordField)?.value;
            if (password && confirmPassword && password !== confirmPassword) {
                return { notSame: true };
            }
        }
        return null;
    };
}

export class ConfirmPasswordStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl): boolean {
        const invalidCtrl = !!(control && control.invalid && control.touched);
        const invalidParent = !!(control && control.parent && control.dirty && control.parent.invalid
            && control.parent.dirty && control.parent.hasError('notSame'));
        return invalidCtrl || invalidParent;
    }
}
