import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Management } from 'src/app/models/management.model';
import { AlertService } from 'src/app/services/alert.service';
import { BatchService } from 'src/app/services/batch.service';
import { ManagementService } from 'src/app/services/management.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-batch-create',
  templateUrl: './batch-create.component.html',
  styleUrls: ['./batch-create.component.css']
})
export class BatchCreateComponent extends FormController implements OnInit{

  managements: Management[] = [];

  tipoDocumento: string = '';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private batchService: BatchService,
    private managementService: ManagementService
  ) {
    super(['description', 'name', 'management']);
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      management: ['', [Validators.required]]
    });


  }

  ngOnInit(){
    this.findManagements();
  }

  findManagements(){
    this.managementService.findAll().then(r => {
      this.managements = r;
    });
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      const batch = await this.batchService.register(this.form.value);
      this.router.navigate(['..', batch.id], { relativeTo: this.route });
      this.alertService.success('Lote criada com sucesso!');
    }
  }

}

