import { JSONModel } from '../utils/json.model';

export class Management extends JSONModel {
  id?: number;
  description?: string | null;
  dateManagement?: number;
  timeCourse?: string | null;
  vacineId?: number;
  userId?: number;
  vacineName?: string | null;

}

export interface IManagement {
  description?: string | null;
  dateManagement?: number;
  timeCourse?: string | null;
  vacineId?: number;
  userId?: number;
}
