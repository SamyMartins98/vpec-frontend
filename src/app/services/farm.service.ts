import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Farm, IFarm } from '../models/farm.model';
import { IPaginationResponse } from '../utils/pagination.model';

const PATH = 'farms';

@Injectable({
  providedIn: 'root'
})
export class FarmService {

  constructor(
    private http: HttpClient
  ) { }

  async register(payload: any): Promise<Farm> {
    const response: IFarm = await this.http.post<IFarm>(`${environment.API_URL}/${PATH}`, payload).toPromise();
    return new Farm(response);
  }

  async all(params?: any): Promise<IPaginationResponse<IFarm>> {
    return await this.http.get<IPaginationResponse<IFarm>>(`${environment.API_URL}/${PATH}`, { params }).toPromise();
  }

  async get(id: number): Promise<Farm> {
    const response: IFarm = await this.http.get<IFarm>(`${environment.API_URL}/${PATH}/${id}`).toPromise();
    return new Farm(response);
  }

  async update(id: number, payload: any): Promise<Farm> {
    const response: IFarm = await this.http.put<IFarm>(`${environment.API_URL}/${PATH}/${id}`, payload).toPromise();
    return new Farm(response);
  }

  async activate(id: number): Promise<Farm> {
    const response: IFarm = await this.http.put<IFarm>(`${environment.API_URL}/${PATH}/${id}/active`, null).toPromise();
    return new Farm(response);
  }

  async inactive(id: number): Promise<Farm> {
    const response: IFarm = await this.http.put<IFarm>(`${environment.API_URL}/${PATH}/${id}/desactive`, null).toPromise();
    return new Farm(response);
  }

}
