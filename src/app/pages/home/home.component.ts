import { MediaMatcher } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { RoleService } from 'src/app/services/role.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  menu: any[];
  mediaQueryList: MediaQueryList;
  openMenu: boolean;
  user?: User;

  constructor(
    private mediaMatcher: MediaMatcher,
    private authService: AuthService,
    private roleService: RoleService,
  ) {
    this.mediaQueryList = this.mediaMatcher.matchMedia('(max-width: 600px)');
    this.menu = this.roleService.getMenu();
    this.openMenu = (localStorage.getItem(`openMenu-${this.authService.user?.email}`) || 'true') === 'true';
    this.user = this.authService.user;
  }

  logout(): void {
    this.authService.logout();
  }

  saveMenuState(event: boolean): void {
    this.openMenu = event;
    localStorage.setItem(`openMenu-${this.authService.user?.email}`, this.openMenu.toString());
  }
}
