import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Animal, IAnimal } from '../models/animal.model';
import { IPaginationResponse } from '../utils/pagination.model';

const PATH = 'animals';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {

  constructor(private http: HttpClient) {
  }

  async register(payload: any): Promise<Animal> {
    const response: IAnimal = await this.http.post<IAnimal>(`${environment.API_URL}/${PATH}`, payload).toPromise();
    console.log(`${environment.API_URL}/${PATH}`);
    return new Animal(response);
  }

  async all(params?: any): Promise<IPaginationResponse<IAnimal>> {
    return await this.http.get<IPaginationResponse<IAnimal>>(`${environment.API_URL}/${PATH}`, { params }).toPromise();
  }

  async get(id: number): Promise<Animal> {
    const response: IAnimal = await this.http.get<IAnimal>(`${environment.API_URL}/${PATH}/${id}`).toPromise();
    return new Animal(response);
  }

  async update(id: number, payload: any): Promise<Animal> {
    const response: IAnimal = await this.http.put<IAnimal>(`${environment.API_URL}/${PATH}/${id}`, payload).toPromise();
    return new Animal(response);
  }

}
