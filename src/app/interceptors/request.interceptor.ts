import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { AlertService } from '../services/alert.service';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    constructor(
        private authService: AuthService,
        private alertService: AlertService,
        private loading: NgxSpinnerService,
        private router: Router
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const token = this.authService.token;
        if (token) {
            request = request.clone({ setHeaders: { Authorization: `Bearer ${token}` } });
        }

        this.loading.show();
        return next.handle(request).pipe(
            catchError(error => {
                if (error.status === 401) {
                    this.authService.logout();
                    this.router.navigate(['/', 'auth']);
                }
                this.alertService.serverError(error);
                throw error;
            }),
            finalize(() => this.loading.hide())
        );
    }
}
