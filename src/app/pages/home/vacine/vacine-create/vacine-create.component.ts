import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { VacineType } from 'src/app/models/vacine.model';
import { AlertService } from 'src/app/services/alert.service';
import { VacineService } from 'src/app/services/vacine.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-vacine-create',
  templateUrl: './vacine-create.component.html',
  styleUrls: ['./vacine-create.component.css']
})
export class VacineCreateComponent extends FormController implements OnInit {

  vacineTypes: VacineType[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private vacineService: VacineService
  ) {
    super(['name','vacineType','dateDueDate']);
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      vacineType: ['', [Validators.required]],
      dateDueDate: ['', [Validators.required]]
    });


  }


  ngOnInit(){
    this.findVacineTypes();
  }

  findVacineTypes(){
    this.vacineService.getAllVacineTypes().then(r => {
      this.vacineTypes = r;
    });
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      const vacine = await this.vacineService.register(this.form.value);
      this.router.navigate(['..', vacine.id], { relativeTo: this.route });
      this.alertService.success('Vacina criada com sucesso!');
    }
  }

}
