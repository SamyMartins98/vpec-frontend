import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Permission } from '../utils/types';
import { AuthService } from './auth.service';

const PERMISSIONS: Permission = {
  home: { name: 'Página Inicial', permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  user: { name: 'Usuários', permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  farm: { name: 'Fazendas', permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  vacine: { name: 'Vacinas', permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO']},
  management: { name: 'Manejos', permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  batch: { name: 'Lotes', permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  animal: { name: 'Animais', permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'user/create': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'user/:id': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'farm/create': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'farm/:id': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'vacine/create': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'vacine/:id': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'management/create': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'management/:id': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'batch/create': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'batch/:id': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'animal/create': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] },
  'animal/:id': { permissions: ['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO'] }
};

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  hasUserTypes(route: string | undefined): boolean {
    const user = this.authService.user;
    if (user && route) {
      const entry = PERMISSIONS[route];
      if (entry && entry.permissions) {
        return user.hasUserTypes(entry.permissions);
      }
    }
    return false;
  }

  getMenu(): any[] {
    const menu = [];
    for (const route in PERMISSIONS) {
      if (PERMISSIONS[route]) {
        const name = PERMISSIONS[route].name;
        if (name && this.hasUserTypes(route)) {
          menu.push({ name, link: route });
        }
      }
    }
    return menu;
  }

  goToFirstPage(): void {
    for (const route in PERMISSIONS) {
      if (this.hasUserTypes(route)) {
        this.router.navigate(['/', 'home', route]);
        break;
      }
    }
  }
}
