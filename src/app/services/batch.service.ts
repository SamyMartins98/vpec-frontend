import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Batch, IBatch } from '../models/batch.model';
import { IPaginationResponse } from '../utils/pagination.model';

const PATH = 'batchs';

@Injectable({
  providedIn: 'root'
})
export class BatchService {

  constructor(private http: HttpClient) {
  }

  async register(payload: any): Promise<Batch> {
    const response: IBatch = await this.http.post<IBatch>(`${environment.API_URL}/${PATH}`, payload).toPromise();
    console.log(`${environment.API_URL}/${PATH}`);
    return new Batch(response);
  }

  async all(params?: any): Promise<IPaginationResponse<IBatch>> {
    return await this.http.get<IPaginationResponse<IBatch>>(`${environment.API_URL}/${PATH}`, { params }).toPromise();
  }

  async get(id: number): Promise<Batch> {
    const response: IBatch = await this.http.get<IBatch>(`${environment.API_URL}/${PATH}/${id}`).toPromise();
    return new Batch(response);
  }

  async update(id: number, payload: any): Promise<Batch> {
    const response: IBatch = await this.http.put<IBatch>(`${environment.API_URL}/${PATH}/${id}`, payload).toPromise();
    return new Batch(response);
  }

  async findAll(): Promise<Batch[]> {
    return await this.http.get<Batch[]>(`${environment.API_URL}/${PATH}/all`).toPromise();
  }

  async findId(id?: number): Promise<Batch> {
    return await this.http.get<Batch>(`${environment.API_URL}/${PATH}/byId/${id}`).toPromise();
  }
}
