import { Component, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormController } from 'src/app/utils/form.controller';
import { FileValidator } from 'ngx-material-file-input';
import { Constants } from 'src/app/utils/constants';

@Component({
  selector: 'app-token-dialog',
  templateUrl: './token-dialog.component.html',
  styleUrls: ['./token-dialog.component.css']
})
export class TokenDialogComponent extends FormController {
  showCertificate: boolean = false;
  hasCertificate: boolean = false;
  hidePassword: boolean = true;
  config: any = {
    length: 6,
    inputStyles: {
      width: '60px',
      height: '60px',
      'margin-top': '1.8em',
      'margin-bottom': '1.8em'
    }
  };

  constructor(
    private dialogRef: MatDialogRef<TokenDialogComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private data: { hasCertificate: boolean }
  ) {
    super(['name', 'description', 'file']);
    this.form = this.formBuilder.group({
      token: ['', [Validators.required, Validators.minLength(6)]],
      file: [undefined, [FileValidator.maxContentSize(Constants.FILE_UPLOAD_MAX_SIZE)]],
      password: ['']
    });
    this.hasCertificate = this.data?.hasCertificate || false;
  }

  onSubmit(): void {
    let certificate = null;
    const file = this.f?.file?.value?.files[0];
    const password = this.f?.password?.value;
    if (file && password && this.showCertificate) {
      certificate = { file, password };
    }
    this.dialogRef.close({ token: this.f.token.value, certificate });
  }

  onInputChange(input: string): void {
    this.f.token.setValue(input);
  }

  toggleCertificate(): void {
    this.showCertificate = !this.showCertificate;
    if (this.showCertificate) {
      this.f.file.setValidators([FileValidator.maxContentSize(Constants.FILE_UPLOAD_MAX_SIZE), Validators.required]);
      this.f.password.setValidators([Validators.required]);
    } else {
      this.f.file.setValidators([FileValidator.maxContentSize(Constants.FILE_UPLOAD_MAX_SIZE)]);
      this.f.password.setValidators([]);
    }
    this.f.file.updateValueAndValidity();
    this.f.password.updateValueAndValidity();
  }

  togglePassword(): void {
    this.hidePassword = !this.hidePassword;
  }
}
