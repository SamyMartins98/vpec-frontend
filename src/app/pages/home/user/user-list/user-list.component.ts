import { Component, ElementRef, ViewChild } from '@angular/core';

import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { DataTableItems } from '../../shared/data-table/data-table.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent {

  title: string = 'Usuários';
  data: DataTableItems[];
  canCreate?: boolean;
  canShow?: boolean;

  constructor(
    public service: UserService,
    private authService: AuthService
  ) {
    this.data = [
      { name: 'id', field: 'id', type: 'text', showOnMobile: true },
      { name: 'nome', field: 'name', type: 'text', showOnMobile: true },
      { name: 'email', field: 'email', type: 'text' },
      { name: 'ativo', field: 'activeInative', type: 'boolean' }

    ];
    this.canCreate = this.authService.user?.hasUserTypes(['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO']);
    this.canShow = this.authService.user?.hasUserTypes(['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO']);
  }




}
