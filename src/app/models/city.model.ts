import { JSONModel } from "../utils/json.model";

export class City extends JSONModel {
  id?: number;
  name?: string | null;
  state?: number | null;

}

export interface ICity {
  name?: string | null;
  state?: number | null;
}
