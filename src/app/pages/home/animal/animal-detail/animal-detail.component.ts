import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from 'src/app/models/animal.model';
import { Batch } from 'src/app/models/batch.model';
import { IdentificationType } from 'src/app/models/identificationtype.model';
import { AlertService } from 'src/app/services/alert.service';
import { AnimalService } from 'src/app/services/animal.service';
import { AuthService } from 'src/app/services/auth.service';
import { BatchService } from 'src/app/services/batch.service';
import { IdentificationTypeService } from 'src/app/services/identificationtype.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-animal-detail',
  templateUrl: './animal-detail.component.html',
  styleUrls: ['./animal-detail.component.css']
})
export class AnimalDetailComponent extends FormController implements OnInit {

  animal!: Animal;
  animalId: number;
  canEdit?: boolean;
  canShow?: boolean;
  batch!: Batch;
  batchName?: string | null;
  identification!: IdentificationType;
  identificationName?: string | null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private animalService: AnimalService,
    private batchService: BatchService,
    private identificationService: IdentificationTypeService,
    private authService: AuthService
  ) {
    super([
      'id',
      'sex',
      'birth',
      'nameColor',
      'qtFirstWeight',
      'dateFirstWeight',
      'castrated',
      'farmInput',
      'coat',
      'descriptionDeathOrLoss',
      'dateDeathOrLoss',
      'identificationType',
      'batchName',
      'identificationName'
    ]);
    this.animalId = this.route.snapshot.params.id;
   }
   async ngOnInit(): Promise<void> {
    try {
      this.animal = await this.animalService.get(this.animalId);
      this.batch = await this.batchService.findId(this.animal.batch);
      this.identification = await this.identificationService.findId(this.animal.identificationType);
      this.batchName = this.batch.name;
      this.identificationName = this.identification.name;
    } catch (error) {
      this.router.navigate(['..'], { relativeTo: this.route });
    }
    this.updateForm();
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      this.animal = await this.animalService.update(this.animalId, this.form.value);
      this.updateForm();
      this.alertService.success('Dados atualizados com sucesso!');
    }
  }

  isDeathOrLoss(){
    if(this.animal.descriptionDeathOrLoss != null)
      return true
    else
      return false;
  }

  updateForm(): void {
    this.form = this.formBuilder.group({
      id: [{ value: this.animal.id, disabled: true }, [Validators.required]],
      sex: [{ value: this.animal.sex, disabled: !this.canEdit }, [Validators.required]],
      birth: [{ value: this.animal.birth, disabled: !this.canEdit }, [Validators.required]],
      nameColor: [{ value: this.animal.nameColor, disabled: !this.canEdit }, [Validators.required]],
      qtFirstWeight: [{ value: this.animal.qtFirstWeight, disabled: !this.canEdit }, [Validators.required]],
      dateFirstWeight: [{ value: this.animal.dateFirstWeight, disabled: !this.canEdit }, [Validators.required]],
      castrated: [{ value: this.animal.castrated, disabled: this.canEdit }, [Validators.required]],
      farmInput: [{ value: this.animal.farmInput, disabled: !this.canEdit }, [Validators.required]],
      coat: [{ value: this.animal.coat, disabled: this.canEdit }, [Validators.required]],
      descriptionDeathOrLoss: [{ value: this.animal.descriptionDeathOrLoss, disabled: this.canEdit }, [Validators.required]],
      dateDeathOrLoss: [{ value: this.animal.dateDeathOrLoss, disabled: this.canEdit }, [Validators.required]],
      identificationType: [{ value: this.animal.identificationType, disabled: !this.canEdit }, [Validators.required]],
      batchName: [{ value: this.batchName, disabled: !this.canEdit }, [Validators.required]],
      identificationName: [{ value: this.identificationName, disabled: !this.canEdit }, [Validators.required]]
    });
  }

}
