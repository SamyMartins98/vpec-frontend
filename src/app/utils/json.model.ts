export class JSONModel {
    parse(json: any): JSONModel {
        const instance: any = this;

        for (const prop in json) {
            if (!json.hasOwnProperty(prop)) { continue; }

            const data = json[prop];
            if (typeof data === 'object' && data != null && !Array.isArray(data)) {
                instance[prop] = this.parse(data);
            } else {
                instance[prop] = data;
            }
        }

        return instance;
    }

    constructor(data?: any) {
        if (data) {
            this.parse(data);
        }
    }
}
