import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

type DialogData = {
  title?: string,
  field?: string,
  cancel?: string,
  ok?: string
};

@Component({
  selector: 'app-input-dialog',
  templateUrl: './input-dialog.component.html',
  styleUrls: ['./input-dialog.component.css']
})
export class InputDialogComponent {
  title: string = '';
  field: string = '';
  cancel: string = 'CANCELAR';
  ok: string = 'SALVAR';
  input: string = '';

  constructor(
    private dialogRef: MatDialogRef<InputDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: DialogData
  ) {
    if (this.data?.title !== undefined) {
      this.title = this.data.title;
    }

    if (this.data?.field !== undefined) {
      this.field = this.data.field;
    }

    if (this.data?.cancel) {
      this.cancel = this.data.cancel;
    }

    if (this.data?.ok) {
      this.ok = this.data.ok;
    }
  }

  submit(): void {
    this.dialogRef.close(this.input);
  }
}
