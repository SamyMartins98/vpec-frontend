import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { IManagement, Management } from '../models/management.model';
import { IPaginationResponse } from '../utils/pagination.model';

const PATH = 'managements';

@Injectable({
  providedIn: 'root'
})
export class ManagementService {

  constructor(private http: HttpClient) {
  }

  async register(payload: any): Promise<Management> {
    const response: IManagement = await this.http.post<IManagement>(`${environment.API_URL}/${PATH}`, payload).toPromise();
    console.log(`${environment.API_URL}/${PATH}`);
    return new Management(response);
  }

  async all(params?: any): Promise<IPaginationResponse<IManagement>> {
    return await this.http.get<IPaginationResponse<IManagement>>(`${environment.API_URL}/${PATH}`, { params }).toPromise();
  }

  async get(id: number): Promise<Management> {
    const response: IManagement = await this.http.get<IManagement>(`${environment.API_URL}/${PATH}/${id}`).toPromise();
    return new Management(response);
  }

  async update(id: number, payload: any): Promise<Management> {
    const response: IManagement = await this.http.put<IManagement>(`${environment.API_URL}/${PATH}/${id}`, payload).toPromise();
    return new Management(response);
  }

  async findAll(): Promise<Management[]> {
    return await this.http.get<Management[]>(`${environment.API_URL}/${PATH}/all`).toPromise();
  }

  async findId(id?: number): Promise<Management> {
    return await this.http.get<Management>(`${environment.API_URL}/${PATH}/byId/${id}`).toPromise();
  }

}
