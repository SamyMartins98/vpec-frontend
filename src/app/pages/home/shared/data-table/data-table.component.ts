import { MediaMatcher } from '@angular/cdk/layout';
import { AfterViewInit, ChangeDetectorRef, Component, Input, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { MaskConstants } from 'src/app/utils/mask.constants';
import { DataSource } from '@angular/cdk/table';
import { IPaginationResponse } from 'src/app/utils/pagination.model';

export interface DataTableItems {
  name: string;
  field: string;
  type: 'text' | 'boolean';
  mask?: string;
  showOnMobile?: boolean;
}

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements AfterViewInit, OnDestroy {
  @Input() title: string = '';
  @Input() service: any;
  @Input() data: DataTableItems[] = [];
  @Input() create?: boolean = false;
  @Input() method?: string = 'all';
  @Input() hasDate?: boolean = false;

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  search: string = '';
  initialDate?: Date;
  finalDate?: Date;
  dataSource?: TableDataSource;
  subscriptions: Subscription[] = [];
  columns: string[] = [];
  mediaQueryList: MediaQueryList;

  mask: MaskConstants = new MaskConstants();

  constructor(
    private mediaMatcher: MediaMatcher,
    private cd: ChangeDetectorRef
  ) {
    this.mediaQueryList = this.mediaMatcher.matchMedia('(max-width: 600px)');
    this.mediaQueryList.addEventListener('change', () => this.setColumns());
  }

  ngOnDestroy(): void {
    this.mediaQueryList.removeEventListener('change', () => this.setColumns());
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  async ngAfterViewInit(): Promise<void> {
    if (this.service) {
      this.dataSource = new TableDataSource(this.service, this.method);
      this.dataSource.load();
      this.subscriptions.push(this.dataSource.$counter.subscribe((count: number) => this.paginator.length = count));
      this.subscriptions.push(this.paginator.page.subscribe(() => this.load()));
      this.setColumns();
    }
  }

  load(): void {
    if (this.dataSource) {
      this.dataSource.load(this.paginator.pageIndex, this.paginator.pageSize, this.search, this.initialDate, this.finalDate);
    }
  }

  private setColumns(): void {
    const data = this.data.map(x => x.field);
    const mobileData = this.data.filter(x => x.showOnMobile).map(x => x.field);
    this.columns = this.mediaQueryList.matches && mobileData.length ? mobileData : data;
    this.columns.push('details');
    this.cd.detectChanges();
  }

}

class TableDataSource implements DataSource<any> {
  private dataSubject = new BehaviorSubject<any[]>([]);
  private countSubject = new BehaviorSubject<number>(0);
  public $counter: Observable<number> = this.countSubject.asObservable();

  constructor(
    private service: any,
    private method: string = 'all'
  ) { }

  connect(): Observable<any[]> {
    return this.dataSubject.asObservable();
  }

  disconnect(): void {
    this.dataSubject.complete();
    this.countSubject.complete();
  }

  async load(page: number = 0, size: number = 10, filter: string = '', startDate?: Date, endDate?: Date): Promise<void> {
    const initialDate = parseDate(startDate);
    const finalDate = parseDate(endDate);
    const response: IPaginationResponse<any> = await this.service[this.method]({ page, size, filter, initialDate, finalDate });
    this.dataSubject.next(response.content);
    this.countSubject.next(response.totalElements);
  }
}

function parseDate(date?: Date): string {
  if (date) {
    return appendLeadingZeroes(date.getDate()) + '/' + appendLeadingZeroes(date.getMonth() + 1) + '/' + date.getFullYear();
  } else {
    return '';
  }
}

function appendLeadingZeroes(n: string | number): string {
  if (n <= 9) {
    return '0' + n;
  }
  return n.toString();
}
