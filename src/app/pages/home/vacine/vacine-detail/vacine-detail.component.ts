import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Vacine, VacineType } from 'src/app/models/vacine.model';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { VacineService } from 'src/app/services/vacine.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-vacine-detail',
  templateUrl: './vacine-detail.component.html',
  styleUrls: ['./vacine-detail.component.css']
})
export class VacineDetailComponent extends FormController implements OnInit {

  vacine!: Vacine;
  vacineId: number = 0;
  canEdit?: boolean;
  canShow?: boolean;
  vacineType!: VacineType;
  vacineTypeName?: string | null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private vacineService: VacineService,
    private authService: AuthService
  ) {
    super([
      'id',
      'name',
      'dateDueDate',
      'vacineTypeName',
      'situation'
    ]);
    this.vacineId = this.route.snapshot.params.id;
   }
   async ngOnInit(): Promise<void> {
    try {
      this.vacine = await this.vacineService.get(this.vacineId);
      this.vacineType = await this.vacineService.findVacineType(this.vacine.vacineType);
      this.vacineTypeName = this.vacineType.name;
    } catch (error) {
      this.router.navigate(['..'], { relativeTo: this.route });
    }
    this.updateForm();
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      this.vacine = await this.vacineService.update(this.vacineId, this.form.value);
      this.updateForm();
      this.alertService.success('Dados atualizados com sucesso!');
    }
  }

  async isActiveInactive(): Promise<void> {
    this.vacine = await this.vacineService.activate(this.vacineId);
    this.updateForm();
    this.alertService.success('Vacina disponível!');
  }

  async isSituationDesactive(): Promise<void> {
    this.vacine = await this.vacineService.inactive(this.vacineId);
    this.updateForm();
    this.alertService.warning('Vacina indisponível!');
  }

  updateForm(): void {
    this.form = this.formBuilder.group({
      id: [{ value: this.vacine.id, disabled: true }, [Validators.required]],
      name: [{ value: this.vacine.name, disabled: !this.canEdit }, [Validators.required]],
      dateDueDate: [{ value: this.vacine.dateDueDate, disabled: !this.canEdit }, [Validators.required]],
      situation: [{ value: this.vacine.situation, disabled: this.canEdit }, [Validators.required]],
      vacineTypeName: [{ value: this.vacineTypeName, disabled: !this.canEdit }, [Validators.required]],
    });
  }

}
