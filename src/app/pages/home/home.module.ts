import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxMaskModule } from 'ngx-mask';
import { NgOtpInputModule } from 'ng-otp-input';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { MaterialModule } from 'src/app/modules/material.module';
import { DataTableComponent } from './shared/data-table/data-table.component';
import { ConfirmationDialogComponent } from './shared/confirmation-dialog/confirmation-dialog.component';
import { TokenDialogComponent } from './shared/token-dialog/token-dialog.component';
import { InputDialogComponent } from './shared/input-dialog/input-dialog.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { FarmCreateComponent } from './farm/farm-create/farm-create.component';
import { FarmListComponent } from './farm/farm-list/farm-list.component';
import { FarmDetailComponent } from './farm/farm-detail/farm-detail.component';
import { VacineCreateComponent } from './vacine/vacine-create/vacine-create.component';
import { VacineDetailComponent } from './vacine/vacine-detail/vacine-detail.component';
import { VacineListComponent } from './vacine/vacine-list/vacine-list.component';
import { ManagementCreateComponent } from './management/management-create/management-create.component';
import { ManagementListComponent } from './management/management-list/management-list.component';
import { ManagementDetailComponent } from './management/management-detail/management-detail.component';
import { BatchCreateComponent } from './batch/batch-create/batch-create.component';
import { BatchListComponent } from './batch/batch-list/batch-list.component';
import { BatchDetailComponent } from './batch/batch-detail/batch-detail.component';
import { AnimalCreateComponent } from './animal/animal-create/animal-create.component';
import { AnimalListComponent } from './animal/animal-list/animal-list.component';
import { AnimalDetailComponent } from './animal/animal-detail/animal-detail.component';
import { PaginaInicialComponent } from './pagina-inicial/pagina-inicial.component';

@NgModule({
  declarations: [
    HomeComponent,
    DataTableComponent,
    UserListComponent,
    UserDetailComponent,
    UserCreateComponent,
    ConfirmationDialogComponent,
    TokenDialogComponent,
    InputDialogComponent,
    FarmCreateComponent,
    FarmListComponent,
    FarmDetailComponent,
    VacineCreateComponent,
    VacineDetailComponent,
    VacineListComponent,
    ManagementCreateComponent,
    ManagementListComponent,
    ManagementDetailComponent,
    BatchCreateComponent,
    BatchListComponent,
    BatchDetailComponent,
    AnimalCreateComponent,
    AnimalListComponent,
    AnimalDetailComponent,
    PaginaInicialComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    MaterialModule,
    NgOtpInputModule,
    HomeRoutingModule,
    ReactiveFormsModule,
    NgxMaskModule.forChild(),
  ]
})
export class HomeModule { }
