import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Batch } from 'src/app/models/batch.model';
import { IdentificationType } from 'src/app/models/identificationtype.model';
import { AlertService } from 'src/app/services/alert.service';
import { AnimalService } from 'src/app/services/animal.service';
import { BatchService } from 'src/app/services/batch.service';
import { IdentificationTypeService } from 'src/app/services/identificationtype.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-animal-create',
  templateUrl: './animal-create.component.html',
  styleUrls: ['./animal-create.component.css']
})
export class AnimalCreateComponent extends FormController implements OnInit{

  listBatchs: Batch[] = [];
  listIdentifications: IdentificationType[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private animalService: AnimalService,
    private batchService: BatchService,
    private identificationTypeService: IdentificationTypeService
  ) {
    super([
      'sex',
      'birth',
      'nameColor',
      'qtFirstWeight',
      'dateFirstWeight',
      'farmInput',
      'identificationType',
      'batch'
    ]);
    this.form = this.formBuilder.group({
      sex: ['', [Validators.required]],
      birth: ['', [Validators.required]],
      nameColor: ['', [Validators.required]],
      qtFirstWeight: ['', [Validators.required]],
      dateFirstWeight: ['', [Validators.required]],
      farmInput: ['', [Validators.required]],
      identificationType: ['', [Validators.required]],
      batch: ['', [Validators.required]],
    });


  }

  ngOnInit(){
    this.findBatchs();
    this.findIdentifications();
  }

  findBatchs(){
    this.batchService.findAll().then(r => {
      this.listBatchs = r;
    });
  }

  findIdentifications(){
    this.identificationTypeService.findAll().then(r => {
      this.listIdentifications = r;
    });
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      const animal = await this.animalService.register(this.form.value);
      this.router.navigate(['..', animal.id], { relativeTo: this.route });
      this.alertService.success('Animal criado com sucesso!');
    }
  }

}
