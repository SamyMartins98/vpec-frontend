import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route } from '@angular/router';
import { AlertService } from '../services/alert.service';
import { RoleService } from '../services/role.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate, CanLoad {
  constructor(
    private roleService: RoleService,
    private alertService: AlertService
  ) { }

  canLoad(route: Route): boolean {
    return this.checkPermission(route.path);
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    return this.checkPermission(route.routeConfig?.path);
  }

  checkPermission(route?: string): boolean {
    const permission: boolean = this.roleService.hasUserTypes(route);
    if (!permission) {
      this.alertService.error('Você não tem permissão de acessar isso!');
      this.roleService.goToFirstPage();
    }
    return permission;
  }
}
