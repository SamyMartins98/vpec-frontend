import { Injectable } from '@angular/core';
import { CanLoad, CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class GuestGuard implements CanActivate, CanLoad {
  constructor(
    private authService: AuthService
  ) { }

  canLoad(): boolean {
    return this.isGuest();
  }

  canActivate(): boolean {
    return this.isGuest();
  }

  isGuest(): boolean {
    if (!this.authService.user) {
      return true;
    } else {
      this.authService.goHome();
      return false;
    }
  }
}




