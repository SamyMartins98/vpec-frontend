import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { UserService } from 'src/app/services/user.service';
import { FormController } from 'src/app/utils/form.controller';
import { ConfirmPasswordValidator, ConfirmPasswordStateMatcher } from 'src/app/validators/confirmPassword.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent extends FormController {

  confirmPasswordStateMatcher = new ConfirmPasswordStateMatcher();
  created: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private alertService: AlertService,
  ) {
    super();
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      cpf: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    }, { validators: ConfirmPasswordValidator('password', 'confirmPassword') });
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      await this.userService.register(this.form.value);
      this.alertService.success('Cadastro enviado com sucesso!');
      this.created = true;
    }
  }
}
