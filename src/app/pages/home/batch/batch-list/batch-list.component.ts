import { Component, OnInit } from '@angular/core';
import { Batch } from 'src/app/models/batch.model';
import { AuthService } from 'src/app/services/auth.service';
import { BatchService } from 'src/app/services/batch.service';
import { DataTableItems } from '../../shared/data-table/data-table.component';


@Component({
  selector: 'app-batch-list',
  templateUrl: './batch-list.component.html',
  styleUrls: ['./batch-list.component.css']
})
export class BatchListComponent{

  title: string = 'Lotes';
  data: DataTableItems[];
  canCreate?: boolean;

  constructor(
    public service: BatchService,
    private authService: AuthService
  ) {
    this.data = [
      { name: 'id', field: 'id', type: 'text', showOnMobile: true },
      { name: 'nome', field: 'name', type: 'text', showOnMobile: true },
      { name: 'descrição', field: 'description', type: 'text', showOnMobile: true }

    ];
    this.canCreate = this.authService.user?.hasUserTypes(['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO']);

  }

}
