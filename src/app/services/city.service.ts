import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { City, ICity } from '../models/city.model';
import { IPaginationResponse } from '../utils/pagination.model';

const PATH = 'citys';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(
    private http: HttpClient
  ) { }

  async all(): Promise<City[]> {
    return await this.http.get<City[]>(`${environment.API_URL}/${PATH}/state/1`).toPromise();
  }

  async findId(id?: number): Promise<City>{
    return await this.http.get<City>(`${environment.API_URL}/${PATH}/${id}`).toPromise();
  }

}
