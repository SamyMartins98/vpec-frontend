import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ManagementService } from 'src/app/services/management.service';
import { DataTableItems } from '../../shared/data-table/data-table.component';

@Component({
  selector: 'app-management-list',
  templateUrl: './management-list.component.html',
  styleUrls: ['./management-list.component.css']
})
export class ManagementListComponent {

  title: string = 'Manejos';
  data: DataTableItems[];
  canCreate?: boolean;

  constructor(
    public service: ManagementService,
    private authService: AuthService
  ) {
    this.data = [
      { name: 'id', field: 'id', type: 'text', showOnMobile: true },
      { name: 'descrição', field: 'description', type: 'text', showOnMobile: true },
      { name: 'período', field: 'timeCourse', type: 'text' }

    ];
    this.canCreate = this.authService.user?.hasUserTypes(['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO']);

  }

}
