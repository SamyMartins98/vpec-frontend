import { JSONModel } from '../utils/json.model';

export class Batch extends JSONModel {
  id?: number;
  name?: string | null;
  description?: string | null;
  management?: number;

}

export interface IBatch {
  name?: string | null;
  description?: string | null;
  management?: number;
}
