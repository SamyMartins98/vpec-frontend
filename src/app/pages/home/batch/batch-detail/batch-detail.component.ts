import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Batch } from 'src/app/models/batch.model';
import { Management } from 'src/app/models/management.model';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { BatchService } from 'src/app/services/batch.service';
import { ManagementService } from 'src/app/services/management.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-batch-detail',
  templateUrl: './batch-detail.component.html',
  styleUrls: ['./batch-detail.component.css']
})
export class BatchDetailComponent extends FormController implements OnInit {

  batch!: Batch;
  batchId: number;
  canEdit?: boolean;
  canShow?: boolean;
  management!: Management;
  manejo?: string | null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private batchService: BatchService,
    private managementService: ManagementService,
    private authService: AuthService
  ) {
    super([
      'id',
      'name',
      'description',
      'manejo'
    ]);
    this.batchId = this.route.snapshot.params.id;
   }
   async ngOnInit(): Promise<void> {
    try {
      this.batch = await this.batchService.get(this.batchId);
      this.management = await this.managementService.findId(this.batch.management);
      this.manejo = this.management.description;
    } catch (error) {
      this.router.navigate(['..'], { relativeTo: this.route });
    }
    this.updateForm();
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      this.batch = await this.batchService.update(this.batchId, this.form.value);
      this.updateForm();
      this.alertService.success('Dados atualizados com sucesso!');
    }
  }

  updateForm(): void {
    this.form = this.formBuilder.group({
      id: [{ value: this.batch.id, disabled: true }, [Validators.required]],
      name: [{ value: this.batch.name, disabled: this.canEdit }, [Validators.required]],
      description: [{ value: this.batch.description, disabled: this.canEdit }, [Validators.required]],
      manejo: [{ value: this.manejo, disabled: !this.canEdit }, [Validators.required]]
    });
  }

}
