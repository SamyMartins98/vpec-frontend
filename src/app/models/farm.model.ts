import { JSONModel } from '../utils/json.model';

export class Farm extends JSONModel {
    id?: number;
    approves?: boolean;
    activeInative?: boolean;
    name?: string | null;
    city?: number;

    isActiveInative(): boolean {
        return this.activeInative === true;
    }

    isApproves(): boolean {
        return this.approves === true;
    }

}

export interface IFarm {
    name?: string | null;
    city?: string | null;
    activeInative?: | null;
}
