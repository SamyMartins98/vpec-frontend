import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { User, IUser } from '../models/user.model';
import { IPaginationResponse } from '../utils/pagination.model';
import { UserType } from '../utils/types';

const PATH = 'users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  async register(payload: any): Promise<User> {
    const response: IUser = await this.http.post<IUser>(`${environment.API_URL}/${PATH}`, payload).toPromise();
    return new User(response);
  }

  async all(params?: any): Promise<IPaginationResponse<IUser>> {
    return await this.http.get<IPaginationResponse<IUser>>(`${environment.API_URL}/${PATH}`, { params }).toPromise();
  }

  async get(id: number): Promise<User> {
    const response: IUser = await this.http.get<IUser>(`${environment.API_URL}/${PATH}/${id}`).toPromise();
    return new User(response);
  }

  async update(id: number, payload: any): Promise<User> {
    const response: IUser = await this.http.put<IUser>(`${environment.API_URL}/${PATH}/${id}`, payload).toPromise();
    return new User(response);
  }

  async approves(id: number): Promise<User> {
    const response: IUser = await this.http.put<IUser>(`${environment.API_URL}/${PATH}/${id}/approve`, null).toPromise();
    return new User(response);
  }

  async activate(id: number): Promise<User> {
    const response: IUser = await this.http.put<IUser>(`${environment.API_URL}/${PATH}/${id}/active`, null).toPromise();
    return new User(response);
  }

  async inactive(id: number): Promise<User> {
    const response: IUser = await this.http.put<IUser>(`${environment.API_URL}/${PATH}/${id}/desactive`, null).toPromise();
    return new User(response);
  }

  async getAllUserTypes(): Promise<UserType[]> {
    return await this.http.get<UserType[]>(`${environment.API_URL}/${PATH}`).toPromise();
  }
}
