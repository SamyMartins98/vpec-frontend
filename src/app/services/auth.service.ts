import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { User } from '../models/user.model';
import { Farm } from '../models/farm.model';
import { Vacine } from './../models/vacine.model';
import { UserService } from './user.service';

const PATH = 'auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  jwtPayload: any;
  private _token?: string | null;
  private _user?: User;
  private _farm?: Farm;
  private _vacine?: Vacine;
  usuarioLogado: number | undefined;

  constructor(
    private router: Router,
    private http: HttpClient,
    private userService: UserService
  ) {
    const user = localStorage.getItem('user');
    const farm = localStorage.getItem('farm');
    const vacine = localStorage.getItem('vacine');
    this._token = localStorage.getItem('token');
    this._user = user ? new User(JSON.parse(user)) : undefined;
    this._farm = farm ? new Farm(JSON.parse(farm)) : undefined;
    this._vacine = vacine ? new Vacine(JSON.parse(vacine)) : undefined;
  }

  get token(): string | null | undefined {
    return this._token;
  }

  get user(): User | undefined {
    return this._user;
  }

  get farm(): Farm | undefined {
    return this._farm;
  }

  get vacine(): Vacine | undefined {
    return this._vacine;
  }

  async login(payload: any): Promise<boolean> {
    const request = await this.http.post<any>(`${environment.API_URL}/${PATH}/token`, payload).toPromise();
    this.save(request);
    return await this.goHome();
  }

  async logout(): Promise<boolean> {
    this._token = undefined;
    this._user = undefined;
    this._farm = undefined;
    this._vacine = undefined;
    this.jwtPayload = null;
    localStorage.clear();
    return await this.goHome();
  }

  async requestPassword(email: string): Promise<void> {
    return await this.http.post<void>(`${environment.API_URL}/${PATH}/recovery-password`, { email }).toPromise();
  }

  async changePassword(payload: any): Promise<void> {
    return await this.http.post<void>(`${environment.API_URL}/${PATH}/change-password`, payload).toPromise();
  }

  async goHome(): Promise<boolean> {
    if (this._user) {
      return await this.router.navigate(['/', 'home']);
    } else {
      return await this.router.navigate(['/', 'auth']);
    }
  }

  private save(request: any): void {
    const { access_token, ...user } = request;
    this._token = access_token;
    this._user = new User(user);
    localStorage.setItem('token', access_token);
    localStorage.setItem('user', JSON.stringify(user));
  }

}
