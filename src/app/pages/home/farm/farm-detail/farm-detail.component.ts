import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { City } from 'src/app/models/city.model';
import { Farm } from 'src/app/models/farm.model';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { CityService } from 'src/app/services/city.service';
import { FarmService } from 'src/app/services/farm.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-farm-detail',
  templateUrl: './farm-detail.component.html',
  styleUrls: ['./farm-detail.component.css']
})
export class FarmDetailComponent extends FormController implements OnInit {

  farm!: Farm;
  farmId: number;
  canEdit?: boolean;
  canShow?: boolean;
  city!: City;
  cityName?: string | null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private farmService: FarmService,
    private cityService: CityService,
    private authService: AuthService
  ) {
    super([
      'id',
      'name',
      'cityName'
    ]);
    this.farmId = this.route.snapshot.params.id;
   }
   async ngOnInit(): Promise<void> {
    try {
      this.farm = await this.farmService.get(this.farmId);
      this.city = await this.cityService.findId(this.farm.city);
    } catch (error) {
      this.router.navigate(['..'], { relativeTo: this.route });
    }
    this.updateForm();
    this.cityName = this.city.name;
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      this.farm = await this.farmService.update(this.farmId, this.form.value);
      this.updateForm();
      this.alertService.success('Dados atualizados com sucesso!');
    }
  }

  async isActivateInactive(): Promise<void> {
    this.farm = await this.farmService.activate(this.farmId);
    this.updateForm();
    this.alertService.success('Fazenda ativada!');
  }

  async isDeactivate(): Promise<void> {
    this.farm = await this.farmService.inactive(this.farmId);
    this.updateForm();
    this.alertService.warning('Fazenda inativada!');
  }

  updateForm(): void {
    this.form = this.formBuilder.group({
      id: [{ value: this.farm.id, disabled: true }, [Validators.required]],
      name: [{ value: this.farm.name, disabled: this.canEdit }, [Validators.required]],
      cityName: [{ value: this.cityName, disabled: !this.canEdit }, [Validators.required]],
    });
  }

}
