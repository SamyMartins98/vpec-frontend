import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuard } from 'src/app/guards/role.guard';
import { HomeComponent } from './home.component';
import { RoleService } from 'src/app/services/role.service';
import { UserListComponent } from './user/user-list/user-list.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { FarmListComponent } from './farm/farm-list/farm-list.component';
import { FarmCreateComponent } from './farm/farm-create/farm-create.component';
import { FarmDetailComponent } from './farm/farm-detail/farm-detail.component';
import { VacineListComponent } from './vacine/vacine-list/vacine-list.component';
import { VacineCreateComponent } from './vacine/vacine-create/vacine-create.component';
import { VacineDetailComponent } from './vacine/vacine-detail/vacine-detail.component';
import { ManagementListComponent } from './management/management-list/management-list.component';
import { ManagementCreateComponent } from './management/management-create/management-create.component';
import { ManagementDetailComponent } from './management/management-detail/management-detail.component';
import { BatchListComponent } from './batch/batch-list/batch-list.component';
import { BatchCreateComponent } from './batch/batch-create/batch-create.component';
import { BatchDetailComponent } from './batch/batch-detail/batch-detail.component';
import { AnimalListComponent } from './animal/animal-list/animal-list.component';
import { AnimalCreateComponent } from './animal/animal-create/animal-create.component';
import { AnimalDetailComponent } from './animal/animal-detail/animal-detail.component';
import { PaginaInicialComponent } from './pagina-inicial/pagina-inicial.component';

@Component({ template: '' })
class RedirectComponent {
  constructor(private roleService: RoleService) { this.roleService.goToFirstPage(); }
}

const routes: Routes = [{
  path: '', component: HomeComponent, children: [
    { path: 'redirect', component: RedirectComponent },
    { path: 'user', component: UserListComponent, canActivate: [RoleGuard] },
    { path: 'user/create', component: UserCreateComponent, canActivate: [RoleGuard] },
    { path: 'user/:id', component: UserDetailComponent, canActivate: [RoleGuard] },
    { path: 'farm', component: FarmListComponent, canActivate: [RoleGuard] },
    { path: 'farm/create', component: FarmCreateComponent, canActivate: [RoleGuard] },
    { path: 'farm/:id', component: FarmDetailComponent, canActivate: [RoleGuard] },
    { path: 'vacine', component: VacineListComponent, canActivate: [RoleGuard] },
    { path: 'vacine/create', component: VacineCreateComponent, canActivate: [RoleGuard] },
    { path: 'vacine/:id', component: VacineDetailComponent, canActivate: [RoleGuard] },
    { path: 'management', component: ManagementListComponent, canActivate: [RoleGuard] },
    { path: 'management/create', component: ManagementCreateComponent, canActivate: [RoleGuard] },
    { path: 'management/:id', component: ManagementDetailComponent, canActivate: [RoleGuard] },
    { path: 'batch', component: BatchListComponent, canActivate: [RoleGuard] },
    { path: 'batch/create', component: BatchCreateComponent, canActivate: [RoleGuard] },
    { path: 'batch/:id', component: BatchDetailComponent, canActivate: [RoleGuard] },
    { path: 'animal', component: AnimalListComponent, canActivate: [RoleGuard] },
    { path: 'animal/create', component: AnimalCreateComponent, canActivate: [RoleGuard] },
    { path: 'animal/:id', component: AnimalDetailComponent, canActivate: [RoleGuard] },
    { path: 'home', component: PaginaInicialComponent, canActivate: [RoleGuard] },
    { path: '**', redirectTo: '', pathMatch: 'full' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
