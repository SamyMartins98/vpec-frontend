import { Component, OnInit } from '@angular/core';
import { AnimalService } from 'src/app/services/animal.service';
import { AuthService } from 'src/app/services/auth.service';
import { DataTableItems } from '../../shared/data-table/data-table.component';

@Component({
  selector: 'app-animal-list',
  templateUrl: './animal-list.component.html',
  styleUrls: ['./animal-list.component.css']
})
export class AnimalListComponent{

  title: string = 'Animais';
  data: DataTableItems[];
  canCreate?: boolean;

  constructor(
    public service: AnimalService,
    private authService: AuthService
  ) {
    this.data = [
      { name: 'id', field: 'id', type: 'text', showOnMobile: true },
      { name: 'data de nascimento', field: 'birth', type: 'text' },
      { name: 'cor', field: 'nameColor', type: 'text' },
      { name: 'castrado', field: 'castrated', type: 'boolean' }

    ];
    this.canCreate = this.authService.user?.hasUserTypes(['ROLE_ADMINISTRADOR', 'ROLE_FAZENDEIRO']);

  }
}
