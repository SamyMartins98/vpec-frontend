import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends FormController {

  hidePassword = true;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {
    super();
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      await this.authService.login(this.form.value);
    }
  }

  togglePassword(): void {
    this.hidePassword = !this.hidePassword;
  }
}
