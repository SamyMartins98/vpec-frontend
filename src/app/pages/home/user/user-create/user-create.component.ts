import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { UserService } from 'src/app/services/user.service';
import { FormController } from 'src/app/utils/form.controller';
import { UserType } from 'src/app/utils/types';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})

export class UserCreateComponent extends FormController {

  listUserType: UserType[] = [];
  tipoDocumento: string = '';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private userService: UserService
  ) {
    super(['cpf', 'email', 'name', 'userTypes']);
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      cpf: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      userTypes: ['', [Validators.required]]
    });


  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      const user = await this.userService.register(this.form.value);
      this.router.navigate(['..', user.id], { relativeTo: this.route });
      this.alertService.success('Usuário criado com sucesso!');
    }
  }

}
