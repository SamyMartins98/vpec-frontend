import { FormControl, FormGroup } from '@angular/forms';
import { MaskConstants } from './mask.constants';

export class FormController {
    public form: FormGroup;
    public readonly mask: MaskConstants = new MaskConstants();

    constructor(fields?: string[]) {
        const controls: any = {};
        if (fields) {
            for (const key of fields) {
                controls[key] = new FormControl('');

            }
        }
        this.form = new FormGroup(controls);
    }

    get f(): FormGroup['controls'] { return this.form?.controls; }

    clearForm(): void {
        if (this.form) {
            this.form.reset();
            const constrols = this.form.controls;
            for (const control in constrols) {
                if (constrols[control]) {
                    constrols[control].setErrors(null);
                }
            }
        }
    }
}
