import { JSONModel } from '../utils/json.model';
import { UserType } from '../utils/types';

export class User extends JSONModel {
    id?: number;
    approves?: boolean;
    activeInative?: boolean;
    name?: string | null;
    cpf?: string | null;
    email?: string | null;
    password?: string | null;
    userType?: string | null;
    userTypes?: UserType[];

    constructor(data?: any) {
      super(data);
      this.userTypes = this.userTypes || [];
    }

    isActiveInative(): boolean {
        return this.activeInative === true;
    }

    isApproves(): boolean {
        return this.approves === true;
    }

    hasUserTypes(userTypes: UserType[]): boolean {
      return this.userTypes && userTypes ? this.userTypes.some((userTypes: UserType) => userTypes.includes(userTypes)) : false;
    }
}

export interface IUser {
    name?: string | null;
    cpf?: string | null;
    email?: string | null;
}
