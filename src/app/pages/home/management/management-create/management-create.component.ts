import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Vacine } from 'src/app/models/vacine.model';
import { AlertService } from 'src/app/services/alert.service';
import { ManagementService } from 'src/app/services/management.service';
import { VacineService } from 'src/app/services/vacine.service';
import { FormController } from 'src/app/utils/form.controller';

@Component({
  selector: 'app-management-create',
  templateUrl: './management-create.component.html',
  styleUrls: ['./management-create.component.css']
})
export class ManagementCreateComponent extends FormController implements OnInit{

  vacineIds: Vacine[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private managementService: ManagementService,
    private vacineService: VacineService
  ) {
    super(['name', 'description', 'dateManagement','timeCourse','vacineId']);
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      dateManagement: ['', [Validators.required]],
      timeCourse: ['', [Validators.required]],
      vacineId: ['', [Validators.required]]
    });


  }

  ngOnInit(){
    this.findVacines();
  }

  findVacines(){
    this.vacineService.findAll().then(r => {
      this.vacineIds = r;
    });
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      const management = await this.managementService.register(this.form.value);
      this.router.navigate(['..', management.id], { relativeTo: this.route });
      this.alertService.success('Manejo criado com sucesso!');
    }
  }

}
