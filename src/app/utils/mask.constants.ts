export class MaskConstants {
    [key: string]: any;
    public readonly CPF: string = '000.000.000-00';
    public readonly CNPJ: string = '00.000.000/0000-00';
    public readonly CPF_CNPJ: string = '000.000.000-00||00.000.000/0000-00';
    public readonly PHONE_NUMBER: string = '(00) 0000-0000||(00) 0 0000-0000';
}
