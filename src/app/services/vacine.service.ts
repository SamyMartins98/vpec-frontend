import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Vacine, IVacine, VacineType } from '../models/vacine.model';
import { IPaginationResponse } from '../utils/pagination.model';

const PATH = 'vacines';

@Injectable({
  providedIn: 'root'
})
export class VacineService {

  constructor(private http: HttpClient) {
  }

  async register(payload: any): Promise<Vacine> {
    const response: IVacine = await this.http.post<IVacine>(`${environment.API_URL}/${PATH}`, payload).toPromise();
    return new Vacine(response);
  }

  async all(params?: any): Promise<IPaginationResponse<IVacine>> {
    return await this.http.get<IPaginationResponse<IVacine>>(`${environment.API_URL}/${PATH}`, { params }).toPromise();
  }

  async findAllVacines(): Promise<Vacine[]> {
    return await this.http.get<Vacine[]>(`${environment.API_URL}/${PATH}`).toPromise();
  }

  async get(id?: number): Promise<Vacine> {
    return await this.http.get<Vacine>(`${environment.API_URL}/${PATH}/${id}`).toPromise();
  }

  async update(id: number, payload: any): Promise<Vacine> {
    const response: IVacine = await this.http.put<IVacine>(`${environment.API_URL}/${PATH}/${id}`, payload).toPromise();
    return new Vacine(response);
  }

  async activate(id: number): Promise<Vacine> {
    const response: IVacine = await this.http.put<IVacine>(`${environment.API_URL}/${PATH}/${id}/active`, null).toPromise();
    return new Vacine(response);
  }

  async inactive(id: number): Promise<Vacine> {
    const response: IVacine = await this.http.put<IVacine>(`${environment.API_URL}/${PATH}/${id}/desactive`, null).toPromise();
    return new Vacine(response);
  }

  async getAllVacineTypes(): Promise<VacineType[]> {
    return await this.http.get<VacineType[]>(`${environment.API_URL}/${PATH}/vacinetypes`).toPromise();
  }

  async findAll(): Promise<Vacine[]> {
    return await this.http.get<Vacine[]>(`${environment.API_URL}/${PATH}/all`).toPromise();
  }

  async findId(id: number): Promise<Vacine> {
    return await this.http.get<Vacine>(`${environment.API_URL}/${PATH}/byId/${id}`).toPromise();
  }

  async findVacineType(id?: number): Promise<VacineType> {
    return await this.http.get<VacineType>(`${environment.API_URL}/${PATH}/vacinetypes/${id}`).toPromise();
  }
}
