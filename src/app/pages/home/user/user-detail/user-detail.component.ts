import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { FormController } from 'src/app/utils/form.controller';
import { ConfirmPasswordStateMatcher, ConfirmPasswordValidator } from 'src/app/validators/confirmPassword.validator';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent extends FormController implements OnInit {
  user!: User;
  userId: number;
  userById?: number;
  canEdit?: boolean;
  canShow?: boolean;

  confirmPasswordStateMatcher = new ConfirmPasswordStateMatcher();

  hide = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private userService: UserService,
    private authService: AuthService
  ) {
    super([
      'id',
      'name',
      'cpf',
      'email',
      'userType',
      'password',
      'confirmPassword'
    ]);
    this.userId = this.route.snapshot.params.id;
    this.canEdit = this.authService.user?.hasUserTypes(['ROLE_ADMINISTRADOR']);
    this.canShow = this.authService.user?.hasUserTypes(['ROLE_FAZENDEIRO']);
  }

  async ngOnInit(): Promise<void> {
    try {
      this.user = await this.userService.get(this.userId);
      this.userById = this.user.id;
    } catch (error) {
      this.router.navigate(['..'], { relativeTo: this.route });
    }
    this.updateForm();
  }

  async onSubmit(): Promise<void> {
    if (this.form.valid) {
      this.user = await this.userService.update(this.userId, this.form.value);
      this.updateForm();
      this.alertService.success('Dados atualizados com sucesso!');
    }
  }

  async isActivateInactive(): Promise<void> {
    this.user = await this.userService.activate(this.userId);
    this.updateForm();
    this.alertService.success('Usuário ativado!');
  }

  async isDeactivate(): Promise<void> {
    this.user = await this.userService.inactive(this.userId);
    this.updateForm();
    this.alertService.warning('Usuário inativado!');
  }

  updateForm(): void {
    this.form = this.formBuilder.group({
      id: [{ value: this.user.id, disabled: true }, [Validators.required]],
      name: [{ value: this.user.name, disabled: this.canEdit }, [Validators.required]],
      cpf: [{ value: this.user.cpf, disabled: this.canEdit }, [Validators.required]],
      email: [{ value: this.user.email, disabled: !this.canEdit }, [Validators.required, Validators.email]],
      userType: [{ value: this.user.userType, disabled: this.canEdit}],
      password: [{ value: this.user.password, disabled: !this.canEdit }, [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    }, { validators: ConfirmPasswordValidator('password', 'confirmPassword') });
  }

  isVisible(){
    if(this.user?.userType === 'ROLE_ADMINISTRADOR')
      return true;
    else
      return false;

  }
}
