import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { IdentificationType } from '../models/identificationtype.model';

const PATH = 'identifications';

@Injectable({
  providedIn: 'root'
})
export class IdentificationTypeService {

  constructor(
    private http: HttpClient
  ) { }

  async findAll(): Promise<IdentificationType[]> {
    return await this.http.get<IdentificationType[]>(`${environment.API_URL}/${PATH}`).toPromise();
  }

  async findId(id?: number): Promise<IdentificationType>{
    return await this.http.get<IdentificationType>(`${environment.API_URL}/${PATH}/${id}`).toPromise();
  }

}
